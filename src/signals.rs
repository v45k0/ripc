use nix;
use chan_signal as cs;

pub struct Boss { name: String }
pub struct Worker { name: String }

impl Boss {
    pub fn new(name: String) -> Boss {
        Boss { name: name }
    }

    pub fn send_permission(self, worker_id: nix::libc::pid_t) {
        println!("Boss {} sending permission to worker {}...", self.name, worker_id);
        nix::sys::signal::kill(worker_id, nix::sys::signal::Signal::SIGINT)
            .expect("Failed to send permission");
        println!("Done");
    }
}


impl Worker {
    pub fn new(name: String) -> Worker {
        Worker { name: name }
    }

    pub fn start(&self) {
        let pid = nix::unistd::getpid();
        println!("Worker {} started, pid = {}", self.name, pid);
        println!("Work done. Waiting for permission to finish...");

        //Create a rendezvous channel for handling the chosen signal(s)
        let signal_channel = cs::notify(&[cs::Signal::INT]);

        //Block the channel until a signal was received
        match signal_channel.recv().unwrap() {
            cs::Signal::INT  => println!("Permission granted! Done."),
            _                => println!("Error - invalid permission code!"),
        }
    }
}

use std;
use memmap::Mmap;
use memmap::Protection;
use std::io::prelude::*;
use std::fs::OpenOptions;


static MAP_LEN: u64 = 4096;
static MAP_ID: &'static str = "/tmp/ripc-shared.memory";

pub fn producer() {
    let handle = OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .truncate(true)
            .open(MAP_ID)
            .unwrap();
    handle.set_len(MAP_LEN).unwrap();

    let mut map = Mmap::open(&handle, Protection::ReadWrite).unwrap();
    unsafe { map.as_mut_slice() }.write(b"tum").unwrap();
    println!("Wrote {:?}", b"tum");

    let halt_duration = std::time::Duration::from_millis(1000000000);
    std::thread::sleep(halt_duration);
}

pub fn consumer() {
    let handle = OpenOptions::new()
            .read(true)
            .open(MAP_ID)
            .unwrap();
    let map = Mmap::open(&handle, Protection::Read).unwrap();
    let mut buf: [u8; 4] = [0; 4];
    unsafe {
        let mut mem = map.as_slice();
        mem.read_exact(&mut buf).unwrap();
    }
    println!("Read {:?}", buf);
}

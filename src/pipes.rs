use std;
use libc;
use rand;
use rand::Rng;
use std::io::Read;
use std::io::Write;
use std::path::Path;
use std::process::Stdio;
use std::process::Command;

//nmcli -t -f ssid,rate,signal dev wifi | sed -n 1p | awk -F ':' '{print "Best network is " $1 " with rate " $2 " and signal " $3}'
pub fn anonymous() {
    let nmcli = Command::new("nmcli")
        .args(&["-t", "-f", "ssid,rate,signal", "dev", "wifi"])
        .stdout(Stdio::piped())
        .spawn()
        .expect("Could not execute nmcli");

    let buf = &mut String::new();
    nmcli.stdout.unwrap().read_to_string(buf)
        .expect("Could not read nmcli output");

    let lines: Vec<&str> = buf.split("\n").collect();
    let data: Vec<&str> = lines[0].split(":").collect();
    println!("Best network is {} with rate {} and signal {}",
             data[0], data[1], data[2]);
}

pub fn list_sbin() {
    let ls = Command::new("ls")
        .arg("/usr/local/sbin")
        .stdout(Stdio::piped())
        .spawn().unwrap();

    let buf = &mut String::new();
    ls.stdout.unwrap()
        .read_to_string(buf)
        .expect("Failed to read piped output");
    println!("{}", buf);
}

static PIPE: &'static str = "/tmp/ripc.pipe";

pub fn named_producer() {
    if ! Path::new(PIPE).exists() {
        let filename = std::ffi::CString::new(PIPE).unwrap();
        unsafe {
            libc::mkfifo(filename.as_ptr(), 0o644);
        }
    }
    let mut pipe = std::fs::OpenOptions::new()
        .append(true)
        .open(PIPE)
        .unwrap();

    let mut random = rand::thread_rng();
    let tick = std::time::Duration::from_millis(1000);
    loop {
        let packet = random.gen::<u8>();
        match pipe.write_all(&[packet]) {
            Ok(_) => {
                println!("Produced {}", packet);
                std::thread::sleep(tick);
            }
            Err(_) => {
                println!("Connection closed!");
                break;
            }
        }
    }
}

pub fn named_consumer() {
    let mut pipe = std::fs::OpenOptions::new()
        .read(true)
        .open(PIPE)
        .unwrap();

    loop {
        let mut package: [u8; 1] = [0; 1];
        match pipe.read_exact(&mut package) {
            Ok(_) => {
                println!("Consumed {}", package[0]);
            }
            Err(_) => {
                println!("Connection closed!");
                break;
            }
        }
    }
}

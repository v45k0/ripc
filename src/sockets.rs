use std;
use rand;
use rand::Rng;
use std::io::prelude::*;
use std::net::TcpStream;
use std::net::TcpListener;
use std::os::unix::net::UnixStream;
use std::os::unix::net::UnixListener;

static UNIX_SOCK: &'static str = "/tmp/ripc-unix.sock";

pub fn unix_server() {
    std::fs::remove_file(UNIX_SOCK).unwrap();
    let server = UnixListener::bind(UNIX_SOCK).unwrap();
    println!("Unix domain server started at {}", UNIX_SOCK);

    loop {
        println!("Waiting for a new connection...");
        match server.accept() {
            Ok((ref mut stream, ref addr)) => {
                println!("Accepted client: {:?}", addr);
                let token: String = rand::thread_rng()
                    .gen_ascii_chars()
                    .take(10)
                    .collect();
                stream.write_all(token.as_bytes()).unwrap();
                println!("Sent token {}", token);
            }
            Err(_) => {
                println!("Connection failed. Shutting down.");
                break;
            }
        }
    }
}

pub fn unix_client() {
    let mut stream = UnixStream::connect(UNIX_SOCK).unwrap();
    println!("Connected to the server!");

    let mut response = String::new();
    stream.read_to_string(&mut response).unwrap();
    println!("Received token {}", response);
    println!("Shutting down...");
}

static TCP_SOCK: &'static str = "127.0.0.1:4444";

pub fn echo_server() {
    let server = TcpListener::bind(TCP_SOCK).unwrap();
    println!("Echo server listening on {}", TCP_SOCK);
    loop {
        match server.accept() {
            Ok((ref mut stream, ref addr)) => {
                let message = &mut String::new();
                stream.read_to_string(message).unwrap();
                println!("Client {:?} sent {}", addr, message);
            },

            Err(_) => {
                println!("Connection dropped. Shutting down...");
                break;
            }
        }
    }
}

pub fn echo_client() {
    let mut stream = TcpStream::connect(TCP_SOCK).unwrap();
    println!("Echo client started and connected.");
    println!("Please enter your input:");
    let line: String = read!("{}\n");
    stream.write_all(line.as_bytes()).unwrap();
    stream.flush().unwrap();
}
